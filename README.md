just a quick sketch of how to do constrained optimization using XTB as an ASE calculator. And then calculating the Debye scattering from the resulting trajectory.

Check https://wiki.fysik.dtu.dk/ase/ase/constraints.html for more ways of constraining stuff.
